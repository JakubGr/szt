from django.contrib.admin import site

from advices.models import Advice, AdviceAdmin

site.register(Advice, AdviceAdmin)

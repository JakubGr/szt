from django.template.defaulttags import register


@register.simple_tag
def replace_page_number(request, page_number):
    get_dict = request.GET.copy()
    get_dict['page'] = page_number
    return get_dict.urlencode()

from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from datetime import datetime
from django.contrib.admin import ModelAdmin

class Advice(models.Model):
    title = models.CharField(max_length=300)
    text = models.TextField()
    creation_date = models.DateTimeField(default=datetime.now())
    last_edition_date = models.DateTimeField(default=datetime.now())
    user = models.ForeignKey(User)

class AdviceAdmin(ModelAdmin):
    list_display = ('id', 'title', 'creation_date', 'user_username')
    ordering = ('id',)

    def user_username(self, obj):
        return obj.user.username
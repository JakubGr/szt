# coding=utf-8
from django import forms
from django.forms.utils import ErrorList


class AdviceSearchingForm(forms.Form):
    title = forms.CharField(label='Tytuł', max_length=300, required=False)
    user = forms.CharField(label='Dodane przez', max_length=150, required=False)
    creation_date_from = forms.DateField(label='od', required=False)
    creation_date_to = forms.DateField(label='do', required=False)


class NewAdvice(forms.Form):
    title = forms.CharField(label='Tytuł', max_length=300)
    text = forms.CharField(label='Treść', widget=forms.Textarea)


class EditAdviceForm(forms.Form):
    def __init__(self, method=None, advice=None):
        if method is None:
            super(EditAdviceForm, self).__init__()
        else:
            super(EditAdviceForm, self).__init__(method)

        if advice is not None:
            self.fields['title'] = forms.CharField(label='Tytuł', max_length=300, initial=advice.title)
            self.fields['text'] = forms.CharField(label='Treść', widget=forms.Textarea, initial=advice.text)
        else:
            self.fields['title'] = forms.CharField(label='Tytuł', max_length=300)
            self.fields['text'] = forms.CharField(label='Treść', widget=forms.Textarea)

from django.shortcuts import render, redirect
from datetime import datetime, timedelta
from django.http import HttpResponseNotFound, HttpResponse, HttpResponseForbidden
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.template.defaulttags import register

from advices.models import Advice
from advices.forms import AdviceSearchingForm, NewAdvice, EditAdviceForm


def advices(request):
    advices_list = Advice.objects.all()
    form = None
    has_page_param = False
    if 'page' in request.GET:
        has_page_param = True
    if request.method == 'GET':
        form = AdviceSearchingForm(request.GET)
        if form.is_valid():
            if 'title' in form.changed_data:
                advices_list = advices_list.filter(title__contains=form.cleaned_data['title'])
            if 'user' in form.changed_data:
                advices_list = advices_list.filter(user__username__contains=form.cleaned_data['user'])
            if 'creation_date_from' in form.changed_data:
                advices_list = advices_list.filter(
                    creation_date__gte=form.cleaned_data['creation_date_from'])
            if 'creation_date_to' in form.changed_data:
                advices_list = advices_list.filter(
                    creation_date__lte=form.cleaned_data['creation_date_to'] + timedelta(days=1))

            paginator = Paginator(advices_list, 3)
            page = request.GET.get('page')
            try:
                advices = paginator.page(page)
                page = int(page)
            except PageNotAnInteger:
                advices = paginator.page(1)
                page = 1
            except EmptyPage:
                advices = paginator.page(paginator.num_pages)
                page = paginator.num_pages
    if not form:
        form = AdviceSearchingForm()
    return render(request, 'advices.html',
                  {'form': form, 'advices': advices, 'previous_pages': range(max(1, page - 5), page),
                   'next_pages': range(page + 1, min(page + 6, paginator.num_pages + 1)),
                   'has_page_param': has_page_param})


def details(request, advice_id):
    advice = Advice.objects.filter(id=advice_id).first()
    if advice is None:
        return HttpResponseNotFound()
    return render(request, 'advice_details.html', {'advice': advice})


def add_advice(request):
    if request.user.is_authenticated:
        form = None
        if request.method == "POST":
            form = NewAdvice(request.POST)
            if form.is_valid():
                advice = Advice()
                advice.title = form.cleaned_data['title']
                advice.text = form.cleaned_data['text']
                advice.user = request.user
                advice.save()
                return redirect('advice_details', advice.id)

        if form is None:
            form = NewAdvice()
        return render(request, 'add_advice.html', {'form': form})
    return HttpResponse('Unathorized', status=401)


def edit_advice(request, advice_id):
    if not request.user.is_authenticated:
        return HttpResponse('Unathorized', status=401)

    advice = Advice.objects.filter(id=advice_id).first()
    if advice is None:
        return HttpResponseNotFound()

    if not advice.user == request.user:
        return HttpResponseForbidden()
    form = None
    if request.method == "POST":
        form = EditAdviceForm(request.POST)
        if form.is_valid():
            advice.title = form.cleaned_data['title']
            advice.text = form.cleaned_data['text']
            advice.last_edition_date = datetime.now()
            advice.save()
            return redirect('advice_details', advice.id)

    if form is None:
        form = EditAdviceForm(advice=advice)
    return render(request, 'edit_advice.html', {'form': form, 'advice': advice})


def delete_advice(request, advice_id):
    if not request.user.is_authenticated:
        return HttpResponse('Unathorized', status=401)

    advice = Advice.objects.filter(id=advice_id).first()
    if advice is None:
        return HttpResponseNotFound()

    if not advice.user == request.user:
        return HttpResponseForbidden()

    return render(request, 'delete_advice.html', {'advice_id': advice_id, 'advice_title': advice.title})


def confirm_deleting_advice(request, advice_id):
    if not request.user.is_authenticated:
        return HttpResponse('Unathorized', status=401)

    advice = Advice.objects.filter(id=advice_id).first()
    if not request.user == advice.user:
        return HttpResponseForbidden()

    if advice is None:
        return HttpResponseNotFound()

    advice.delete()
    return redirect('advices')

# coding=utf-8
from django import forms
from django.db.models import QuerySet
from django.forms.utils import ErrorList
from django.utils.safestring import mark_safe

from guitars.models import GUITAR_TYPES, GuitarProducer, GuitarImage


class NewGuitarForm(forms.Form):
    name = forms.CharField(label='Nazwa', max_length=150)
    type = forms.ChoiceField(label='Typ', choices=[type for type in GUITAR_TYPES])
    producer = forms.ChoiceField(label='Producent',
                                 choices=[(producer.id, producer.name) for producer in GuitarProducer.objects.all()])
    description = forms.CharField(label='Opis', widget=forms.Textarea, required=False)
    images = forms.ImageField(label=u'Zdjęcia', widget=forms.ClearableFileInput(attrs={'multiple': True}))


class EditGuitarForm(forms.Form):
    def __init__(self, method=None, files=None, guitar=None):
        if method is None:
            super(EditGuitarForm, self).__init__()
        else:
            super(EditGuitarForm, self).__init__(method, files)

        if guitar is not None:
            self.fields['name'] = forms.CharField(label='Nazwa', max_length=150, initial=guitar.name)
            self.fields['type'] = forms.ChoiceField(label='Typ', choices=[type for type in GUITAR_TYPES],
                                                    initial=guitar.type)
            self.fields['producer'] = forms.ChoiceField(label='Producent',
                                                        choices=[(producer.id, producer.name) for producer in
                                                                 GuitarProducer.objects.all()],
                                                        initial=guitar.producer_id)
            self.fields['description'] = forms.CharField(label='Opis', widget=forms.Textarea, required=False,
                                                         initial=guitar.description)
            old_images = GuitarImage.objects.filter(guitar=guitar)
            self.fields['old_images'] = ImageChoiceField(old_images, label='Obecne zdjęcia',
                                                         widget=forms.CheckboxSelectMultiple, empty_label='',
                                                         initial=[img.id for img in old_images],
                                                         help_text='Po odznaczeniu i zapisaniu zdjęcie zostanie usunięte')
        else:
            self.fields['name'] = forms.CharField(label='Nazwa', max_length=150)
            self.fields['type'] = forms.ChoiceField(label='Typ', choices=[type for type in GUITAR_TYPES])
            self.fields['producer'] = forms.ChoiceField(label='Producent',
                                                        choices=[(producer.id, producer.name) for producer in
                                                                 GuitarProducer.objects.all()])
            self.fields['description'] = forms.CharField(label='Opis', widget=forms.Textarea, required=False)

        self.fields['images'] = forms.ImageField(label=u'Nowe zdjęcia', required=False,
                                                 widget=forms.ClearableFileInput(attrs={'multiple': True}))


class ImageChoiceField(forms.ModelChoiceField):
    def __init__(self, queryset, empty_label="---------", required=True, widget=None, label=None, initial=None,
                 help_text='', to_field_name=None, limit_choices_to=None, *args, **kwargs):
        super(ImageChoiceField, self).__init__(queryset, empty_label, required, widget, label, initial, help_text,
                                               to_field_name, limit_choices_to, *args, **kwargs)

    def label_from_instance(self, obj):
        return mark_safe('<img src="{0}" alt="img" height="100" />'.format(obj.image.url))


class ThumbnailImageChoserForm(forms.Form):
    def __init__(self, method=None, images=None, initial=None):
        if method is None:
            super(ThumbnailImageChoserForm, self).__init__()
        else:
            super(ThumbnailImageChoserForm, self).__init__(method)

        if images is not None:
            self.fields['thumbnail_image'] = ImageChoiceField(label='Główne zdjęcie', queryset=images,
                                                              widget=forms.RadioSelect, empty_label='', initial=initial)

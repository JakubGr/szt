from __future__ import unicode_literals

from django.db import models
from django.db.models.signals import pre_delete
from django.dispatch import receiver
from django.contrib.admin import ModelAdmin

GUITAR_TYPES = (
    (0, 'inna'),
    (1, 'akustyczna'),
    (2, 'klasyczna'),
    (3, 'elektryczna'),
)


class GuitarProducer(models.Model):
    name = models.CharField(max_length=150)


class GuitarProducerAdmin(ModelAdmin):
    list_display = ('id', 'name',)
    ordering = ('id',)


class Guitar(models.Model):
    name = models.CharField(max_length=150)
    description = models.TextField()
    producer = models.ForeignKey(GuitarProducer)
    type = models.IntegerField(default=0, choices=GUITAR_TYPES)
    deleted = models.BooleanField(default=False)


class GuitarAdmin(ModelAdmin):
    list_display = ('id', 'name', 'type')
    ordering = ('id',)


def upload_filename(instance, filename):
    return 'img/guitars/{0}/{1}'.format(instance.guitar.id, filename)


class GuitarImage(models.Model):
    guitar = models.ForeignKey(Guitar)
    image = models.ImageField(upload_to=upload_filename)
    thumbnail_image = models.BooleanField()


@receiver(pre_delete, sender=GuitarImage)
def mymodel_delete(sender, instance, **kwargs):
    instance.image.delete(False)


class GuitarImageAdmin(ModelAdmin):
    list_display = ('id', 'image', 'guitar_name',)
    ordering = ('id',)

    def guitar_name(self, obj):
        return obj.guitar.name

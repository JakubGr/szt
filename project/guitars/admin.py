from django.contrib.admin import site

from guitars.models import Guitar, GuitarProducer, GuitarImage
from guitars.models import GuitarAdmin, GuitarProducerAdmin, GuitarImageAdmin

site.register(Guitar, GuitarAdmin)
site.register(GuitarProducer, GuitarProducerAdmin)
site.register(GuitarImage, GuitarImageAdmin)

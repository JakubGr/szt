# coding=utf-8
from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseForbidden, HttpResponseNotFound

from guitars.models import Guitar, GuitarProducer, GuitarImage, GUITAR_TYPES
from guitars.forms import NewGuitarForm, EditGuitarForm, ThumbnailImageChoserForm

IMAGE_EXTENSIONS = [
    '.jpg',
    '.jpeg',
    '.gif',
    '.png',
    '.bmp',
]


def guitars(request):
    guitar_types = [type[1] for type in GUITAR_TYPES if type[0] != 0]
    types_to_filter = [type[0] for type in GUITAR_TYPES if type[1] in request.GET]
    if not types_to_filter:
        types_to_filter = [type[0] for type in GUITAR_TYPES]

    producers = GuitarProducer.objects.all()
    produers_to_filter = [producer for producer in producers if producer.name in request.GET]
    if not produers_to_filter:
        produers_to_filter = producers

    images = GuitarImage.objects
    guitars_list = [{'guitar': guitar, 'image': images.filter(guitar=guitar, thumbnail_image=True).first()} for guitar
                    in Guitar.objects.filter(type__in=types_to_filter, producer__in=produers_to_filter, deleted=False)]
    return render(request, 'guitars.html',
                  {'guitars': guitars_list, 'guitar_types': guitar_types, 'producers': producers, 'filters': {
                      'types': [type for type in guitar_types if type in request.GET],
                      'producers': [producer.name for producer in producers if producer.name in request.GET],
                  }})


def details(request, guitar_id):
    guitar = Guitar.objects.filter(id=guitar_id).get()
    if guitar.deleted:
        return HttpResponseNotFound()
    images = GuitarImage.objects.filter(guitar__id=guitar_id)

    if 'big_image_id' in request.GET:
        big_image = images.filter(id=request.GET['big_image_id']).get()
    else:
        big_image = images.filter(thumbnail_image=True).first()
    return render(request, 'details.html', {'guitar': guitar, 'images': images, 'big_image': big_image})


def add_guitar(request):
    if request.user.is_authenticated:
        if request.user.is_superuser:
            form = None
            if request.method == 'POST':
                form = NewGuitarForm(request.POST, request.FILES)
                images = request.FILES.getlist('images')
                if form.is_valid():
                    if not [image for image in images if
                            str(image)[str(image).rfind('.'):] in IMAGE_EXTENSIONS] == images:
                        form.add_error('images', 'Nieprawidłowy format zdjęć')
                    else:
                        new_guitar = Guitar()
                        new_guitar.name = form.cleaned_data['name']
                        new_guitar.type = form.cleaned_data['type']
                        new_guitar.producer = GuitarProducer.objects.filter(id=form.cleaned_data['producer']).get()
                        new_guitar.description = form.cleaned_data['description']
                        new_guitar.save()

                        first_image = True
                        for image in images:
                            guitar_image = GuitarImage()
                            if first_image:
                                first_image = False
                                guitar_image.thumbnail_image = True
                            else:
                                guitar_image.thumbnail_image = False
                            guitar_image.guitar = new_guitar
                            guitar_image.image.save(image.name, image)
                        return redirect('guitar_details', new_guitar.id)
            if not form:
                form = NewGuitarForm()
            return render(request, 'add_guitar.html', {'form': form})

        return HttpResponseForbidden()
    return HttpResponse('Unathorized', status=401)


def edit_guitar(request, guitar_id):
    if request.user.is_authenticated:
        if request.user.is_superuser:
            guitar = Guitar.objects.filter(id=guitar_id).get()
            form = None
            if request.method == 'POST':
                form = EditGuitarForm(request.POST, request.FILES)
                images = request.FILES.getlist('images')

                if form.is_valid() and form.changed_data:
                    if not [image for image in images if
                            str(image)[str(image).rfind('.'):] in IMAGE_EXTENSIONS] == images:
                        form.add_error('images', 'Nieprawidłowy format zdjęć')
                    else:
                        guitar.name = form.cleaned_data['name']
                        guitar.type = form.cleaned_data['type']
                        guitar.producer = GuitarProducer.objects.filter(id=form.cleaned_data['producer']).get()
                        guitar.description = form.cleaned_data['description']
                        guitar.save()

                        old_images = GuitarImage.objects.filter(guitar=guitar)
                        for image in [img for img in old_images if str(img.id) not in form.data.getlist('old_images')]:
                            image.delete()

                        for image in images:
                            guitar_image = GuitarImage()
                            guitar_image.thumbnail_image = False
                            guitar_image.guitar = guitar
                            guitar_image.image.save(image.name, image)

                        if not GuitarImage.objects.filter(guitar=guitar, thumbnail_image=True):
                            new_thumbnail_image = GuitarImage.objects.filter(guitar=guitar).first()
                            if new_thumbnail_image:
                                new_thumbnail_image.thumbnail_image = True
                                new_thumbnail_image.save();
                        return redirect('guitar_details', guitar_id)
            if not form:
                form = EditGuitarForm(guitar=guitar)
            return render(request, 'edit_guitar.html', {'form': form, 'guitar': guitar})

        return HttpResponseForbidden()
    return HttpResponse('Unathorized', status=401)


def choose_thumbnail_image(request, guitar_id):
    if request.user.is_authenticated:
        if request.user.is_superuser:
            images = GuitarImage.objects.filter(guitar__id=guitar_id)
            initial = images.filter(thumbnail_image=True).first()
            if images:
                images_exists = True
            else:
                images_exists = False

            form = None
            if request.method == 'POST':
                form = ThumbnailImageChoserForm(method=request.POST)

                if form.is_valid():

                    thumbnail_image_id = form.data['thumbnail_image']
                    if not initial or initial.id != thumbnail_image_id:
                        if initial:
                            initial.thumbnail_image = False
                            initial.save()

                        new_thumbnail_image = images.filter(id=thumbnail_image_id).get()
                        new_thumbnail_image.thumbnail_image = True
                        new_thumbnail_image.save()

                    return redirect('guitar_details', guitar_id)
            if not form:
                if initial:
                    form = ThumbnailImageChoserForm(images=images, initial=initial.id)
                else:
                    form = ThumbnailImageChoserForm(images=images, initial=0)
            return render(request, 'thumbnail_image.html',
                          {'form': form, 'guitar_id': guitar_id, 'images_exists': images_exists})

        return HttpResponseForbidden()

    return HttpResponse('Unathorized', status=401)


def delete_guitar(request, guitar_id):
    if not request.user.is_authenticated:
        return HttpResponse('Unathorized', status=401)

    if not request.user.is_superuser:
        return HttpResponseForbidden()

    guitar = Guitar.objects.filter(id=guitar_id).first()
    if guitar is None:
        return HttpResponseNotFound()

    return render(request, 'delete_guitar.html', {'guitar_name': guitar.name, 'guitar_id': guitar_id})


def confirm_deleting_guitar(request, guitar_id):
    if not request.user.is_authenticated:
        return HttpResponse('Unathorized', status=401)

    if not request.user.is_superuser:
        return HttpResponseForbidden()

    guitar = Guitar.objects.filter(id=guitar_id).first()
    if guitar is None:
        return HttpResponseNotFound()

    guitar.deleted = True
    guitar.save()
    return redirect('guitars')

"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.views.static import serve

from main import views as main_views
from guitars import views as guitar_views
from advices import views as advices_views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', guitar_views.guitars, name='home'),
    url(r'^guitars/$', guitar_views.guitars, name='guitars'),
    url(r'^guitars/details/(?P<guitar_id>\d+)', guitar_views.details, name='guitar_details'),
    url(r'^guitars/new', guitar_views.add_guitar, name='add_guitar'),
    url(r'^guitars/edit/(?P<guitar_id>\d+)', guitar_views.edit_guitar, name='edit_guitar'),
    url(r'^guitars/delete/(?P<guitar_id>\d+)$', guitar_views.delete_guitar, name='delete_guitar'),
    url(r'^guitars/delete/(?P<guitar_id>\d+)/confirm', guitar_views.confirm_deleting_guitar, name='confirm_deleting_guitar'),
    url(r'^guitars/thumbnail_image/(?P<guitar_id>\d+)', guitar_views.choose_thumbnail_image,
        name='choose_thumbnail_image'),
    url(r'^advices/$', advices_views.advices, name='advices'),
    url(r'^advices/(?P<advice_id>\d+)', advices_views.details, name='advice_details'),
    url(r'^advices/new', advices_views.add_advice, name='new_advice'),
    url(r'^advices/edit/(?P<advice_id>\d+)', advices_views.edit_advice, name='edit_advice'),
    url(r'^advices/delete/(?P<advice_id>\d+)$', advices_views.delete_advice, name='delete_advice'),
    url(r'^advices/delete/(?P<advice_id>\d+)/confirm', advices_views.confirm_deleting_advice, name='confirm_deleting_advice'),
    url(r'^accounts/', include('registration.backends.simple.urls')),
    url(r'', include('social.apps.django_app.urls', namespace='social')),
    url(r'^media/(?P<path>.*)', serve, {'document_root': settings.MEDIA_ROOT}),
]

urls = urlpatterns
